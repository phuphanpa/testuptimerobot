terraform {
  required_providers {
    uptimerobot = {
      source  = "louy/uptimerobot"
      version = "0.5.1"
    }
  }
}

provider "uptimerobot" {
  api_key = "u1269524-cadfae73385a4c5ad057217d"
}

data "uptimerobot_account" "account" {}

data "uptimerobot_alert_contact" "default_alert_contact" {
  friendly_name = data.uptimerobot_account.account.email
}

resource "uptimerobot_alert_contact" "phuphanpa1" {
  friendly_name = "phuphanpa1"
  type          = "email"
  value         = "phuphanpa1@gmail.com"
}

resource "uptimerobot_monitor" "google" {
  friendly_name = "google.com"
  type          = "http"
  url           = "https://google.com.vn"
  interval      = 300

  alert_contact {
    id = uptimerobot_alert_contact.phuphanpa1.id
  }

  alert_contact {
    id = data.uptimerobot_alert_contact.default_alert_contact.id
  }

  lifecycle {
    ignore_changes = [
      alert_contact[0].id,
      alert_contact[1].id,
    ]
  }
}

resource "uptimerobot_monitor" "yahoo" {
  friendly_name = "yahoo.com.vn"
  type          = "http"
  url           = "https://vn.yahoo.com/"
  interval      = 300

  alert_contact {
    id = uptimerobot_alert_contact.phuphanpa1.id
  }

  alert_contact {
    id = data.uptimerobot_alert_contact.default_alert_contact.id
  }

  lifecycle {
    ignore_changes = [
      alert_contact[0].id,
      alert_contact[1].id,
    ]
  }
}

resource "uptimerobot_monitor" "pingeu" {
  friendly_name = "ping.eu"
  type = "http"
  url = "https://ping.eu/"
  interval = 300

  alert_contact {
    id = uptimerobot_alert_contact.phuphanpa1.id
  }

  alert_contact {
    id = data.uptimerobot_alert_contact.default_alert_contact.id
  }

  lifecycle {
    ignore_changes = [
      alert_contact[0].id,
      alert_contact[1].id,
    ]
  }
}

resource "uptimerobot_monitor" "bing" {
  friendly_name = "bing"
  type = "http"
  url = "https://www.bing.com/"
  interval = 300

  alert_contact {
    id = uptimerobot_alert_contact.phuphanpa1.id
  }

  alert_contact {
    id = data.uptimerobot_alert_contact.default_alert_contact.id
  }

  lifecycle {
    ignore_changes = [
      alert_contact[0].id,
      alert_contact[1].id,
    ]
  }
}

resource "uptimerobot_monitor" "docker" {
  friendly_name = "docker"
  type = "http"
  url = "https://www.docker.com/"
  interval = 300

  alert_contact {
    id = uptimerobot_alert_contact.phuphanpa1.id
  }

  alert_contact {
    id = data.uptimerobot_alert_contact.default_alert_contact.id
  }

  lifecycle {
    ignore_changes = [
      alert_contact[0].id,
      alert_contact[1].id,
    ]
  }
}

resource "uptimerobot_monitor" "facebook" {
  friendly_name = "facebook"
  type = "http"
  url = "https://www.facebook.com/"
  interval = 300

  alert_contact {
    id = uptimerobot_alert_contact.phuphanpa1.id
  }

  alert_contact {
    id = data.uptimerobot_alert_contact.default_alert_contact.id
  }

  lifecycle {
    ignore_changes = [
      alert_contact[0].id,
      alert_contact[1].id,
    ]
  }
}


resource "uptimerobot_monitor" "hub-docker-com/u/phuphan11" {
  friendly_name = "phuphan11"
  type = "http"
  url = "https://hub.docker.com/u/phuphan11"
  interval = 300

  alert_contact {
    id = uptimerobot_alert_contact.phuphanpa1.id
  }

  alert_contact {
    id = data.uptimerobot_alert_contact.default_alert_contact.id
  }

  lifecycle {
    ignore_changes = [
      alert_contact[0].id,
      alert_contact[1].id,
    ]
  }
}

